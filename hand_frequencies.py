from gamemaster import card
from itertools  import combinations

number_of_ranks          = len('23456789TJQKA')

### Each of the hand types bellow may be contained by the types bellow it. Hence we first get the number of ###
### those types that may be contained and we subtract that from the number of those that may contain them.  ###
                                                                
# ROYAL FLUSH    #
hands_containing_royalflush = 4                 # one Royal Flush per suit
# STRAIGHT FLUSH #
hands_containing_straightflushes = ((1 +(number_of_ranks+1) - len('TJQKA')) * 4) - hands_containing_royalflush
# FLUSH          #
hands_containing_flushes     = 4 * len(list(combinations([i for i in range(number_of_ranks)], 5))) - \
                               (hands_containing_straightflushes + hands_containing_royalflush)# select 5 cards from each suit for 4 suits
# STRAIGHT       #
hands_containing_straights   = (1 + (number_of_ranks+1) - len('TJQKA')) * (4**len('TJQKA')) - \
                               (hands_containing_straightflushes + hands_containing_royalflush) 
                               
number_of_possible_pairs = number_of_ranks * len(list(combinations([i for i in range(4)], 2))) 
number_of_possible_trips = number_of_ranks * len(list(combinations([i for i in range(4)], 3)))

# FULL HOUSE     #
hands_containing_fullhouse   = int(number_of_possible_trips * ((number_of_possible_pairs/number_of_ranks) * 12))
# QUADS          #
number_of_possible_quads     = number_of_ranks   # one Quad per rank (3C,3H,3D,3S), (4C,4H,4D,4S), etc.
hands_containing_quads       = number_of_possible_quads * (52-4) # 52-4 number of cards in deck minus number already extracted. 
                                                                 # Hand rank is invariant under the addition of a card.
# TRIPS          #
# 52 - 3 (number of cards drawn) - 1 (card that completes the trips to a quad)
hands_containing_trips       = (number_of_possible_trips * len(list(combinations([i for i in range(52-4)], 2)))) - \
                               (hands_containing_fullhouse)
# TWO PAIR       #
# 0.5 below serves to cut out duplicates (e.g. 2S2D3S3D5H <-> 3S3D5H2S2D
hands_containing_twopairs    = int((number_of_possible_pairs) * \
                                   ((number_of_possible_pairs/number_of_ranks) * (number_of_ranks-1)) * 0.5) * \
                                   (52-4-4)
# PAIRS          #
hands_containing_pairs       = number_of_possible_pairs * len(list(combinations([i for i in range(52-2-2)], 3))) - \
                               (hands_containing_twopairs*2 + hands_containing_fullhouse)
# HIGHS          #
hands_containing_highs      = len(list(combinations([i for i  in range(52)], 5))) - \
                              sum((hands_containing_pairs, hands_containing_twopairs, hands_containing_trips, \
                                  hands_containing_straights, hands_containing_flushes, hands_containing_fullhouse, \
                                  hands_containing_quads, hands_containing_straightflushes, hands_containing_royalflush))
#def rawHandScore(hand):

if __name__ == '__main__':  
    for item in [hands_containing_highs,      hands_containing_pairs,      hands_containing_twopairs,        \
                hands_containing_trips,       hands_containing_straights,  hands_containing_flushes,         \
                hands_containing_fullhouse,   hands_containing_quads,      hands_containing_straightflushes, \
                hands_containing_royalflush]:
        print(item)
