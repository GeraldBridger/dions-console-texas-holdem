'''

Determine the relative strengths of all possible hands in Texas Hold'Em poker.

Note: words expressed in  CAPITAL_LETTERS and joined with underscored
      denote global constants.
      
'''

from hand_comparison_algorithms import *

from collections import OrderedDict
from itertools   import combinations


###########################
##  Global Constants     ##
###########################

# calculated by choose(52, 5)       #
NUMBER_OF_POSSIBLE_5CARD_HANDS = 2598960

# Number of hands lower than a given hand by virtue of 
# its rank (ignoring lower hands within the same rank)
LOWER_HAND_FREQUENCIES = OrderedDict([
    ('High'           , 0       ),
    ('Pair'           , 1302540 ),
    ('Two Pair'       , 2400780 ),
    ('Trip'           , 2524332 ),
    ('Straight'       , 2579244 ),
    ('Flush'          , 2589444 ),
    ('Full House'     , 2594552 ),
    ('Quad'           , 2598296 ),
    ('Straight Flush' , 2598920 ),
    ('Royal Flush'    , 2598956 )
])


##############################################################
##  Wrapper Function for rank-specific algorithms           ##
##############################################################
    
def countWeakerHands(hand):
    ''' 
    
    Return number of hands weaker than the given hand.
    The functions called from this wrapper reside in weakerhand_algorithms.py
    
    Note On Input Formatting:
          
        hand.cards must be formatted so card precedence mirrors the importance
        of a given card in breaking a tie between two hands of the same rank.
          
        E.g. [3C, 3S, 3D, AH, AC] for a full house, since trips are compared to
              break a tie before dubs. Junk cards that don't influence the rank
              of a hand are thus expected to appear at the end of the hand.
    
    '''
    
    if not hand.rank:
        hand.findRank()
        
    rank       = hand.rank
    cards      = hand.hand
      
    return {
    
        'Royal Flush'       :  lambda dummy: 0              ,
        'Straight Flush'    :  countWeakerStraightFlushes   ,
        'Quad'              :  countWeakerQuads             ,
        'Full House'        :  countWeakerFullHouses        ,
        'Flush'             :  countWeakerFlushes           ,
        'Straight'          :  countWeakerStraights         ,
        'Trip'              :  countWeakerTrips             ,
        'Two Pair'          :  countWeakerTwoPairs          ,
        'Pair'              :  countWeakerPairs             ,
        'High'              :  countWeakerHighs
        
    }[rank](cards) + LOWER_HAND_FREQUENCIES[rank]

    
def percentageScore(hand):
    '''
    
    calculate the fraction of hands weaker than the given hand against all possible hands.
    
    '''
    return hand.strength / NUMBER_OF_POSSIBLE_5CARD_HANDS
    
if __name__ == "__main__":

    import hand_strength_test.py    
    
    
    