import itertools
from math        import factorial
from collections import namedtuple
from random      import random

card = namedtuple('card', 'rank suit')

def choose(n, r):
    '''Return n choose r'''
    if r  > n: return 0
    if r == 0: return 1
    num   = factorial(n)
    denum = factorial(r) * factorial(n-r)
    return int(num/denum)
    
def orderedDeck(SUITS='CDHS', RANKS='23456789TJQKA'):
    '''Build a deck of ordered cards.'''
    return [card(rank, suit) for suit in SUITS for rank in RANKS]
    
def makeHandFromString(a_string):
    '''Convert string representing a hand of cards into a list of card objects'''
    a_string = ''.join(a_string.split())
    return [ card(rank=a_string[i], suit=a_string[i+1]) 
               for i in range(0, len(a_string), 2)     ]
    
def makeStringFromHand(hand):
    '''Convert a list of hand objects into a human-readable string that represents the hand.'''
    return ' '.join([card.rank + card.suit for card in hand])
    
def weightedRandom(weight):
    '''
    Return a random number selected from a weighted distribution.
    '''
    return (random()*weight + random() + random() + random())/4



def weightedRandom2(center, squeeze):
    answer = random()
    

def histo(a_list):
    '''
    print a histogram representing the frequency of numbers in a list.
    numbers are presumed to be between 1 and 0.
    '''
    result = {i: 0 for i in range(10)}
    for item in a_list:
        x = int(item * 10)
        result[x] += 1
    for i in range(10):
        print('#'*int(result[i]/100))

if __name__ == '__main__':
    #x = [weightedRandom(1.1) for i in range(10000)]
    #y = [z for z in x if int(z*10) < 10]
    y = [weightedRandom(1.3) for i in range(10000)]
    print(y[:10])
    histo(y)
    
