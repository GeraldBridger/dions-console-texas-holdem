#!/usr/bin/python3
# -*- coding: utf-8 -*-

#############################################
#                                           #
#  Module handling all aspects of gameplay  # 
#                                           #
#############################################
from collections import OrderedDict
from gamescreen  import GameScreen
from gamemaster  import GameMaster
from player      import Player
                      
def main():
    player_names = ['Abraham', 'Isaac', 'Jacob']
    # PLAYERS = OrderedDict([(player_name, Player(name=player_name)) \
    #                        for player_name in player_names])
    # PLAYERS['Dion'] = Player(name='Dion', is_human=True)
    pname = input("Please enter your name: ")
    PLAYERS = [Player(name=p) for p in player_names] + \
              [Player(name=pname, is_human=True)]
    game_screen = GameScreen('Dion Bridger\'s Texas Hold\'Em\t\t"Ctrl+D" to Quit')
    #for item in PLAYERS: game_screen.addPlayerMarquee(PLAYERS[item])
    for item in PLAYERS: game_screen.addPlayerMarquee(item)
    game = GameMaster(game_screen, PLAYERS, delay = 0.5)
    game_screen.draw()    
    game.RoundMaster()
    print('Thanks for playing!! Email suggestions and/or excoriations to dgbridger@gmx.co.uk')
    return 1

if __name__=='__main__':
    import sys
    sys.exit(main())
