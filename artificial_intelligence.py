#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

Module for building and modelling A.I. Players for Texas Hold'Em

'''

from random import random
from random import randrange
from hand_comparison_algorithms import countWeakerRanks

ranks_precedence = {
    'Royal Flush' : 0, 'Straight Flush': 1, 
    'Quad'        : 2, 'Full House'    : 3,
    'Flush'       : 4, 'Straight'      : 5,
    'Trip'        : 6, 'Two Pair'      : 7,
    'Pair'        : 8, 'High'          : 9    
}

len_ranks = len(ranks_precedence)

class Brain:
    


    def __init__(self, aggressiveness = 1):
        self.money      = 0
        self.hand_score = 0
        self.bet        = 0

        self.aggressiveness = aggressiveness



    def getInformation(self, player, game):
        
        player.hand.findRank()
        player.hand.findScore()
        curr_hand_rank = player.hand.rank
        
        naive_score = ((len_ranks - ranks_precedence[curr_hand_rank])/len_ranks)
        total_score = (player.hand.score + naive_score)/2
        
        self.hand_score = total_score



    def tryActions(self, desirable_actions, possible_actions):
        '''
        
        Auxiliary to getChoice()
        Order in which desirable_actions are given matters, since 
        the first desirable action that is possible will be returned.

        '''
        
        for action in desirable_actions:
            if action in possible_actions:
                return action
                
        return False



    def randomizeChoice(self):
        '''

        Return a random number suitable for adding noise to player choices.
        Auxiliary to self.getChoice()

        '''
        return randrange(7, 12)/10 # Place holder




    def getChoice(self, player, game_state, possible_actions):
        '''

        Determine the most appropriate course of action for a player.

        '''
        self.getInformation(player, game_state)

        if len(player.hand) != 2:
            score  = self.hand_score * self.aggressiveness * self.randomizeChoice()
        else:
            score = countWeakerRanks(player.hand.cards_list[0].rank) * \
                    countWeakerRanks(player.hand.cards_list[1].rank) 
            score /= 12**2;
#            score *= 0.6 # lol magic numbers

        result = None

        if score < 0.4:
            result = self.tryActions(['Check', 'Fold'],         
                                     possible_actions)
        elif 0.4 < score < 0.6:
            result = self.tryActions(['Call',  'Bet', 'Check', 'Raise', 'Fold'], 
                                     possible_actions)
        elif 0.6 < score < 0.9:
            result = self.tryActions(['Raise', 'Bet', 'Call','Check','Fold'],  
                                     possible_actions)
        elif 0.9 < score:
            result = self.tryActions(['All in','Check','Fold'], possible_actions)

        # determine bet to made if any
        # Something's amiss here...
        if result in ('Bet', 'Raise'):
            mira = game_state.minimum_raise
            # magic numbers
            self.bet = int(player.money/randrange(3,7) * self.hand_score)
            # ensure minimum raise is met
            if self.bet < mira:
                if player.money >= mira:
                    self.bet = mira
                else: self.bet = player.money
                

        return result
        
        

    def getBet(self):
        ''' 
        
        Wrapper method for accessing the appropriate bet 
        as determined by self.getChoice()

        '''
        return self.bet



if __name__ == '__main__':
    from utilities import makeHandFromString
    from player    import Player

    dummy_player = Player('Nobody', 1000, False)
    dummy_brain  = Brain() 


    def constructHandList():

        hand = list()
        
        # Royal Flush
        hand.append(makeHandFromString('JH 7H 6H AH 5C 4H TH KH 5H 3H QH 2H 6S'))
        
        # Straight Flush
        hand.append(makeHandFromString('JH 7H 6H AH 5C 4H KH 5H 3H QH 2H 6S'))    

        # Quad
        hand.append(makeHandFromString('JH 7H 6H 7D 5C 4H 7S 5H 3C 7C 2H 6S'))   

        # Full House
        hand.append(makeHandFromString('JH 6H 6H 7D 5C 4H 7S 5H 3C 7C 2H 6S'))   

        # Flush
        hand.append(makeHandFromString('JH 6H 6H 7D 5C 4H AS 5H 3C 7C 2H'))   

        # Straight
        hand.append(makeHandFromString('JH 6H 6D 7D 5C 4H AS 5H 3C 7C 2C'))      

        # Trips
        hand.append(makeHandFromString('JH 6C 6D 5C 6H AS 4H 7C 2D'))             

        # Two Pair
        hand.append(makeHandFromString('JH 6C 6D 5C 4H AS 4H 7C 2D'))            

        # Pair
        hand.append(makeHandFromString('JH 6C 6D 5C 3H AS 4H 8C 9D'))

        # High
        hand.append(makeHandFromString('AH KH QH JH 9S 4D 8C 5D'))            
        hand.append(makeHandFromString('2C 3C 4C 5C AD'))
        hand.append(makeHandFromString('2C 3C 4C 9C QD'))
        hand.append(makeHandFromString('2C 3C 4C 5C 7D'))
        hand.append(makeHandFromString('2C 3C 4C 5C 8D'))

        return hand


    def getInformation__test__():

        '''

        test Brain.getInformation()

        '''
        hand_list = constructHandList()

        for hand in hand_list:
            for card in hand:
                dummy_player.hand.addCard(card)
            dummy_brain.getInformation(dummy_player, None)
            print(dummy_brain.hand_score)
            dummy_player.hand.resetHand()

    def getChoice__test__():
        '''

        test Brain.getChoice()

        '''
        hand_list = constructHandList()
        possible_actions = ['Fold', 'Check', 'Bet', 'Call', 'Raise', 'All in']
        
        for hand in hand_list:
            for card in hand:
                dummy_player.hand.addCard(card)
            action = dummy_brain.getChoice(dummy_player, None, possible_actions)
            print(action, dummy_brain.bet if action in ('Bet', 'Raise') else '')
            dummy_player.hand.resetHand()



    def constructPairHandsList():
        hand = list()
        ranks = '23456789TJQKA'

        for i in ranks:
            for j in ranks:
                hand.append(makeHandFromString(i+'S '+j+'S'))
        return hand

            
    def getChoicePairs__test__():
        '''

        test Brain.getChoice() when player has pairs.

        '''
        hand_list = constructPairHandsList()
        poss_actions = ['Fold', 'Check', 'Bet', 'Call', 'Raise', 'All in']
        for hand in hand_list:
            for card in hand:
                dummy_player.hand.addCard(card)
            chosen_action = dummy_brain.getChoice(dummy_player, None, poss_actions)
            print(hand[0].rank, hand[1].rank, chosen_action, dummy_brain.bet if chosen_action in ('Bet', 'Raise') else '')
            dummy_player.hand.resetHand()

    

    ### Run Tests ###

    #getInformation__test__()
    getChoice__test__()
    #getChoicePairs__test__()
    
