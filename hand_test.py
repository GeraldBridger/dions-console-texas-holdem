from hand import *
from utilities import makeHandFromString
from utilities import makeStringFromHand

example = Hand()

def testHand(a_hand):
    a_hand = makeHandFromString(a_hand)
    for card in a_hand:
        example.addCard(card)
#        print(example.ranks_by_count)
#        print(example.counts_by_rank)
    example.findRank()
    print(example.rank)
    print(makeStringFromHand(example.hand))
    example.findStrength()
    print(example.strength)
    example.findScore()
    print(example.score, '\n')
    example.resetHand()
from time import time

x1 = time()
    
a_hand = 'JH 7H 6H AH 5C 4H TH KH 5H 3H QH 2H 6S'   # Royal Flush
testHand(a_hand)

a_hand = 'JH 7H 6H AH 5C 4H KH 5H 3H QH 2H 6S'      # Straight Flush
testHand(a_hand)

a_hand = 'JH 7H 6H 7D 5C 4H 7S 5H 3C 7C 2H 6S'      # Quad
testHand(a_hand)

a_hand = 'JH 6H 6H 7D 5C 4H 7S 5H 3C 7C 2H 6S'      # Full House
testHand(a_hand)

a_hand = 'JH 6H 6H 7D 5C 4H AS 5H 3C 7C 2H'         # Flush
testHand(a_hand)

a_hand = 'JH 6H 6D 7D 5C 4H AS 5H 3C 7C 2C'         # Straight
testHand(a_hand)

a_hand = 'JH 6C 6D 5C 6H AS 4H 7C 2D'               # Trips
testHand(a_hand)

a_hand = 'JH 6C 6D 5C 4H AS 4H 7C 2D'               # Two Pair
testHand(a_hand)

a_hand = 'JH 6C 6D 5C 3H AS 4H 8C 9D'               # Pair
testHand(a_hand)

a_hand = 'AH KH QH JH 9S 4D 8C 5D'                   # High
testHand(a_hand)

x2 = time()

print(x2-x1)
