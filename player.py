'''

Here we find routines for handling attributes that pertain to players
such as the amount of money they currently have, what cards they have
in their hand, what their AI routines look like (if applicable), etc.

'''

import random
from hand import Hand

from artificial_intelligence import Brain

class Player(object):

    '''

    Routines and attributes specific to a given (not necessarily human)
    player.  

    This is basically a wrapper class for the hand module and the AI module.

    '''


    def __init__(self, name, money=25, is_human=False):

        self.name  = name
        self.money = money
        self.hand  = Hand()

        self.total_bet = 0
        self.is_human  = is_human
        self.status = 'not bust'

        if not is_human:
            self.ai_brain = Brain()

        self.position = 0



    def giveCard(self, card):
        ''' 

        Store the card in the player's hand 

        '''

        self.hand.addCard(card)


    
    def takeCards(self, card):
        '''

        Clear player's hand and return his cards.

        '''

        self.hand.resetHand()



    def makeChoice(self, game_state, possible_actions):
        '''

        Get choice either by prompting human player or 
        by calling a.i. routines and return it.

        '''

        if self.is_human:
            choice = input("Choose an option: ")
        else:
            # personality defines how we operate on
            # the information available to the AI
            #self.ai_brain.getChoice(self, game_state, possible_actions) 
            choice = self.ai_brain.getChoice(self, game_state, possible_actions)
        return choice        



    def makeBet(self, game_state, minimum_raise, raising=False):
        '''

        Get a bet either by prompting human player or 
        by calling a.i. routines and return it.

        '''

        # debugging detritus
        # print("last player's bet: {}; minimum_raise: {}; raising: {}".format(
        #      game_state.last_players_bet, minimum_raise, raising))
        # input("...")

        if self.is_human:
            bet = ''
            while True:
                bet = input("Minimum raise: {}; Bet: ".format(minimum_raise))
                if bet.isdigit() and not (int(bet) < minimum_raise and raising):
                    break
            return int(bet)
        else:
            #return self.ai_brain.getBet(self, game_state)
            return self.ai_brain.getBet()

    

    ''' ~= Methods for handling player actions =~ '''

    def getPossibleActions(self, game_state):
        '''

        Returns list of strings describing options available the player.

        '''
        # Get possible actions 
        has_money = self.money > 0

        can_check = self.total_bet == game_state.last_players_bet             \
                    or not has_money
        
        can_raise = self.money > game_state.minimum_raise

        can_bet   = game_state.players.index(self) == game_state.first_better \
                    and can_raise
        
        # STRONGLY think this ought to be referencing the *total* amount betted 
        # by the last player rather than the amount betted in the last round
        # ...which it in fact *does*
        can_call  = game_state.last_players_bet > self.total_bet and          \
                    game_state.last_players_bet < self.money + self.total_bet
        
        # Construct list of available options
        possible_actions  = ['Check' ] * can_check \
                         or ['Fold'  ] * has_money
        possible_actions += ['Call'  ] * can_call
        possible_actions += ['Bet'   ] * can_bet   \
                         or ['Raise' ] * can_raise
        possible_actions += ['All in'] * has_money
        
        return possible_actions



    def buildActionsDict(self, actions):
        '''

            Map commands available to player to multiple 
            possible inputs that could be used to call it.
            Auxiliary to getPlayerAction

        '''
        result = {}

        # 'Fold' is always mapped to by 0 to so user doesn't begin to associate '1' 
        # with 'Check' and then he accidentally folds when '1' maps to 'Fold'
        j = int(not 'Fold' in actions) 

        for i in range(len(actions)):
            result[str(i+j)          ] = actions[i]
            result[actions[i]        ] = actions[i]
            result[actions[i].lower()] = actions[i]
            result[actions[i].upper()] = actions[i]

        return result



    def getAction(self, game_state, game_screen):
        '''

        Present a player with possible actions and effectuate his choice.
        Auxiliary to placeBets.

        '''

        possible_actions      = self.getPossibleActions(game_state)
        possible_actions_dict = self.buildActionsDict(possible_actions)

        if self.is_human:
            game_screen.drawAvailableActions(self, possible_actions)

        choice = None
        while not choice in possible_actions_dict:
            # print(choice, possible_actions)
            # input("<><>")
            choice = self.makeChoice(game_state, possible_actions)

        return possible_actions_dict[choice]
