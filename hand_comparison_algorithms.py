'''

Algorithms for finding the number of hands weaker
than a given hand, but which share the same rank.

Auxiliary module to hand_strength.py

Note: names given in CAPITAL_LETTERS and joined 
      using underscores denote global constants.

      function comments with words surrounded by {curly braces}
      denote arguments and variables defined for that function.

'''

from utilities   import choose
from collections import OrderedDict


###########################
##  Global Constants     ##
###########################

CARD_ORDINALITY  = OrderedDict(zip(['2','3','4','5','6','7','8','9','T','J','Q','K','A'], 
                                   [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12]))
                                   
NUM_OF_SUITS     = len(('Clubs', 'Hearts', 'Diamonds', 'Spades'))
NUM_OF_RANKS     = len(CARD_ORDINALITY)                                   

SUIT_COMBINATIONS_5CARDS = (NUM_OF_SUITS**5) - NUM_OF_SUITS


###########################
##  Auxiliary Functions  ##
###########################
                                   
def countWeakerRanks(rank, ace_high=True):
    '''
    Count the total number of ranks weaker than the given {rank}.
    '''
    result = CARD_ORDINALITY[rank]
    if not ace_high:
        result = (result + 1) % NUM_OF_RANKS
    return result

def countWeakerKickers(ranks, excluded_ranks=[]):
    '''
    Convert a hand of cards into its rank-wise comparison number.
    Score is set relative to other hands ** of the same length **.
    
    Input must be rank-ordered.
    '''

    ranks = [card.rank for card in ranks]
    x  = 0

    for i in range(len(ranks)):
        weaker_ranks  = countWeakerRanks(ranks[i])
        weaker_ranks -= len([r for r in excluded_ranks if r < ranks[i]])
        x += choose(weaker_ranks, len(ranks)-i)
        
    return x 
   
    
##############################################################
##  Rank-specific algorithms for calculating number of      ##
##  lower-scoring hands with the same rank as a given hand  ##
##############################################################



def countWeakerStraightFlushes(cards):
    '''
    
    Count the number of straight flushes scoring lower than cards.
    
    '''
    
    weakest_card_rank = cards[-1].rank

    # One straight for each rank weaker than weakest card's rank
    # (got by appending next four strongest ranks to weaker ranks)
    weaker_straights_counted = countWeakerRanks(weakest_card_rank)

    # Every straight forms a flush in all four suits.
    result = weaker_straights_counted * NUM_OF_SUITS
    
    return result
   
   
   
def countWeakerQuads(cards):
    '''
    
    Given a quad, count the number of lower-scoring quads.
    
    Expected format of {cards}: "quad"       + "kicker"
                                [A, B, C, D] + [E]

    '''
    quad_rank   = cards[ 0].rank
    kick_rank   = cards[-1].rank
        
    # There is no way to join a kicker to a quad 
    # such that the resultant hand is not a quad.
    
    """  Hold quad constant, vary kickers  """
    rank_combinations  = countWeakerRanks(kick_rank) - (quad_rank < kick_rank)
    
    """  Vary quads and vary kickers       """
    rank_combinations  = countWeakerRanks(quad_rank, 4) * (NUM_OF_RANKS - 1)
        
    # choose(4,4) = 1, choose(4,1) = 4 = NUM_OF_SUITS
    return rank_combinations * NUM_OF_SUITS

    
    
def countWeakerFullHouses(cards):
    '''
    
    Given a full house, count the number of lower-scoring full houses.

    Expected format of {cards}: "triple"  + "double"
                                [A, B, C] + [D, E]

    '''
    trip_rank = cards[ 0].rank
    doub_rank = cards[-1].rank
    
    rank_combinations = 0
    suit_combinations = choose(NUM_OF_SUITS, 3) * choose(NUM_OF_SUITS, 2)
    
    # When {trip_rank} < {doub_rank}, we uncount doubles sharing 
    # {trip_rank}, because 5 cards of the same rank isn't possible.
    if CARD_ORDINALITY[trip_rank] < CARD_ORDINALITY[doub_rank]:
        rank_combinations -= 1    
    
    """ Hold triple constant, vary doubles """
    weaker_doub_ranks = countWeakerRanks(doub_rank)
    rank_combinations += weaker_doub_ranks
        
    """ Vary triples as well as doubles """
    # As above, we need to pick doubles that don't share {trip_rank}.
    rank_combinations += countWeakerRanks(trip_rank) * (NUM_OF_RANKS-1)
  
    return rank_combinations * suit_combinations
   
   
   
def countWeakerFlushes(cards):
    '''
    
    Given a flush, count the number of lower-scoring flushes.
    
    The cards must be rank-ordered from high to low.
    
    '''
    
    hand_len = 5
    result   = 0
    
    # For each card in {cards}, holding all cards not to its right constant 
    # and varying all cards to its right gives the number of flushes weaker 
    # than {cards} which can be built using all those cards held constant.
    for i in range(1,hand_len+1):
        # Hold the first 5-i cards constant, vary the rest
        weaker_ranks = countWeakerRanks(cards[-i].rank)
        result += choose(weaker_ranks, i) * NUM_OF_SUITS
    
    return result 
   


def countWeakerStraights(cards):
    '''
    
    Given a straight, count the number of lower scoring straights.
    
    '''

    weakest_card_rank = cards[-1].rank
    
    # One straight weaker than {cards} exists for 
    # every card weaker than the weakest card
    weaker_cards_counted = countWeakerRanks(weakest_card_rank) 
    
    # And the cards of each such straight can adopt different suits 
    # independently of one another so long as no flushes are made
    result = weaker_cards_counted * SUIT_COMBINATIONS_5CARDS
    
    return result
    
    
    
def countWeakerTrips(cards):
    '''
    
    Given a hand containing trips, count the number 
    of lower scoring hands containing trips.
    
    '''
    
    trip_rank = cards[0  ].rank
    kicks     = cards[-2:]
    
    # No pair of kickers joined to a trip can form a flush.
    rank_combinations  = 0    
    suit_combinations  = choose(NUM_OF_SUITS, 3) * (NUM_OF_SUITS**2)
        
    """ Hold trip constant, vary kickers """
    rank_combinations += countWeakerKickers(kicks, excluded_ranks=[trip_rank])
    
    """ Vary trips                       """
    # Avoid choosing a kicker that forms a quad or full house.
    kickers_rank_combinations = (NUM_OF_RANKS-1) * (NUM_OF_RANKS-2)
    rank_combinations += countWeakerRanks(trip_rank) * kickers_rank_combinations
    
    return rank_combinations * suit_combinations
    
    
    
def countWeakerTwoPairs(cards):
    '''
    
    Given a hand containing two pairs, count the number 
    of lower scoring hands also containing two pairs.
    
    Expected format of {cards}: "double + "double" + "kicker"
                                [A, B]  + [C, D]   +  [E]
                                
    '''
    
    kicker_rank  = cards[-1].rank
    hi_pair_rank = cards[ 0].rank
    lo_pair_rank = cards[ 2].rank
    
    rank_combinations = 0                                             
    suit_combinations = choose(NUM_OF_SUITS, 2)**2 * NUM_OF_SUITS
    
    # There's no way to join cards to any pairs such that the resulting hand 
    # forms a flush, since the suits on all pairs are necessarily different.
    # We only avoid picking kickers that form a trip with one of the 2 pairs.
    kickers_ranks     = NUM_OF_RANKS - 2
    
    """ Hold higher pair and weaker pair constant, vary kicker """
    compareRanks = lambda r1,r2: CARD_ORDINALITY[r1] < CARD_ORDINALITY[r2]
    rank_combinations   += countWeakerRanks(kicker_rank)            - \
                           compareRanks(hi_pair_rank, kicker_rank)  - \
                           compareRanks(lo_pair_rank, kicker_rank)
    
    """ Hold higher pair constant, vary weaker pair and kickers """
    rank_combinations   += countWeakerRanks(lo_pair_rank)  * kickers_ranks
        
    """ Vary higher pair, weaker pair and kickers               """
    weaker_pairs_counted = countWeakerRanks(hi_pair_rank)
    rank_combinations   += choose(weaker_pairs_counted, 2) * kickers_ranks
    
    return rank_combinations * suit_combinations


    
def countWeakerPairs(cards):
    ''' 
    
    Given a hand with rank 'pair', count number of hands 
    with that rank which are weaker than the given hand.
    
    Expected format of {cards}: "double + "kickers"
                                [A, B]  + [C, D, E]
    
    '''
    
    pair_rank    = cards[  0].rank
    kickers      = cards[-3:]
    
    result       = 0
    weaker_ranks = 0
    
    # straights can't be constructed from pairs.
    # Hence we only need to avoid quads, trips and pairs.
       
    ''' Hold pair constant, vary kickers '''
    weaker_ranks += countWeakerKickers(kickers, excluded_ranks=[pair_rank])
    
    ''' Vary pair as well as the kickers '''
    kicker_pool   = NUM_OF_RANKS - 1
    weaker_pairs  = countWeakerRanks(pair_rank)
    weaker_ranks += weaker_pairs * choose(kicker_pool, 3)

    return weaker_ranks * choose(NUM_OF_SUITS, 2) * NUM_OF_SUITS**3
    
    


    
def countWeakerHighs(cards):
    '''  
    
    Give a high-card hand, count lower-scoring high-card hands.
    
    {cards} should be rank-ordered from high to low.
    
    '''
    
    result                   = 0
    
    # << COUNT HANDS WITH WEAKER CARDS >>
    # We have one "weaker rank combination" for every rank-series
    # that is weaker than the given hand of {cards}. For example,
    # 2C 3C, 2C 3H,...,2H 3H are all of equal ranking and thus we
    # represent them all simultaneously by the rank series 2X 3X.
    # High-card hands are ranked as if the hand is ALL KICKERS!!!
    weaker_rank_combinations = countWeakerKickers(cards)
    
    # << REMOVE STRAIGHTS >>
    # 7 6 5 3 2  <  7 6 5 4 3 
    # Only cards *lower* than high-card can form the head of a 
    # straight which contains lower ranking cards than {cards}.
    # Also note that there aren't enough ranks to make a straight using any of
    # 5X, 4X, 3X, 2X as highest card. 5X can't be used as AX is then the head.
    possible_straight_heads  = countWeakerRanks(cards[0].rank) - len((5,4,3,2)) 
    
    if possible_straight_heads > -1:
        weaker_rank_combinations -= possible_straight_heads
    if cards[0].rank == 'A': # AX 5X 4X 3X 2X is a straight! very sneaky bug :)
        weaker_rank_combinations -= 1
    
    # << COUNT SUIT COMBINATIONS >>
    result += weaker_rank_combinations * SUIT_COMBINATIONS_5CARDS
    
    return result

    
###########################
##       Testing         ##
###########################
    
if __name__ == '__main__':
    
    from utilities import makeHandFromString

    ####
    ##      NOTE: The following tests are just meant to be sanity checks, not rigorous unit-tests.
    ####
    
    
    ## Testing countWeakerHighs

    hand_strings = [
        # lower edge case
        "7X 5X 4X 3X 2X", "7X 6X 4X 3X 2X", "7X 6X 5X 3X 2X", "7X 6X 5X 4X 2X", #"7X 6X 5X 4X 3X", # straight
        "8X 5X 4X 3X 2X", "8X 6X 4X 3X 2X", "8X 6X 5X 3X 2X", "8X 6X 5X 4X 2X",
        "8X 6X 5X 4X 3X", "8X 7X 4X 3X 2X", "8X 7X 5X 3X 2X", "8X 7X 5X 4X 2X", 
        "8X 7X 5X 4X 3X",
        "AX KX QX TX 2X", "AX KX QX TX 3X", "AX KX QX TX 4X", "AX KX QX TX 5X", # Upper edge case
        "AX KX QX TX 6X", "AX KX QX TX 7X", "AX KX QX TX 8X", "AX KX QX TX 9X", 
        "AX KX QX JX 2X", "AX KX QX JX 3X", "AX KX QX JX 4X", "AX KX QX JX 5X",
        "AX KX QX JX 6X", "AX KX QX JX 7X", "AX KX QX JX 8X", "AX KX QX JX 9X"  
    ]
    
    for hand_string in hand_strings: 
        hand = makeHandFromString(hand_string)
        print(hand_string)
        print("Weaker hands: ", countWeakerHighs(hand))
        print("Weaker ranks: ", int(countWeakerHighs(hand)/SUIT_COMBINATIONS_5CARDS), '\n\n')
    

    ## Testing countWeakerPairs
    
    hand_strings = [
        "2C 2C 5X 4X 3X", "2C 2C 6X 4X 3X", "2C 2C 6X 5X 3X", "2C 2C 6X 5X 4X", 
        "2C 2C 7X 4X 3X", "2C 2C 7X 5X 3X", "2C 2C 7X 5X 4X", "2C 2C 7X 6X 3X", 
    ]
    
    for hand_string in hand_strings:
        print(hand_string)
        hand = makeHandFromString(hand_string)
        print("Weaker pairs: ", countWeakerPairs(hand))
        print("Weaker ranks: ", int(countWeakerPairs(hand)/(choose(NUM_OF_SUITS, 2) * NUM_OF_SUITS**3)), '\n\n')

        
    ## Testing countWeakerPairs
    
    hand_strings = [
        "3C 3H 2D 2S 4C", "3C 3H 2D 2S 5C", "3C 3H 2D 2S 6C", "3C 3H 2D 2S 7C", # Testing kicker incrementation
        "3C 3H 2D 2S 8C", "3C 3H 2D 2S 9C", "3C 3H 2D 2S TC", "3C 3H 2D 2S JC", 
        "3C 3H 2D 2S QC", "3C 3H 2D 2S KC", "3C 3H 2D 2S AC", 
        '4C 4H 2C 2C 3C', '4C 4H 2C 2C 5C', '4C 4H 2C 2C 6C', '4C 4H 2C 2C 7C', # Testing kicker incrementation 
        '4C 4H 2C 2C 8C', '4C 4H 2C 2C 9C', '4C 4H 2C 2C TC', '4C 4H 2C 2C JC', # with varying higher pair
        '4C 4H 2C 2C QC', '4C 4H 2C 2C KC', '4C 4H 2C 2C AC',
        '5C 5H 2C 2C 3C', '5C 5H 2C 2C 4C', '5C 5H 2C 2C 6C', '5C 5H 2C 2C 7C', # Testing kicker incrementation 
        '5C 5H 2C 2C 8C', '5C 5H 2C 2C 9C', '5C 5H 2C 2C TC', '5C 5H 2C 2C JC', # with varying both pairs
        '5C 5H 2C 2C QC', '5C 5H 2C 2C KC', '5C 5H 2C 2C AC',

    ]
    z = [[0]]
    for hand_string in hand_strings:
        print(hand_string)
        hand = makeHandFromString(hand_string)
        z.append([countWeakerTwoPairs(hand), hand_string])
        print("Weaker two_pairs: ", z[-1][0],'Difference from previous_pair',z[-1][0] - z[-2][0],'\n')


        