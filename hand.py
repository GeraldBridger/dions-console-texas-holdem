'''

    Module for storing and manipulating hands composed of cards.

'''

from itertools                  import chain
from collections                import defaultdict
from hand_comparison_algorithms import countWeakerRanks
from hand_strength              import countWeakerHands
from hand_strength              import percentageScore
from utilities                  import makeStringFromHand


class Hand:
    
    '''
        Handles all hand stuff.
    '''
    
    def __init__(self):
        
        self.RANKS = 'AKQJT98765432'
        self.SUITS = 'CHDS'

        self.cards_list = list()
        
        self.rank  = None
        self.hand  = None
        self.strength = 'not_set'
        self.score    = 'not_set'

        self.resetHand()



    def __len__(self):
        return len(self.cards_list)
      


    def resetHand(self):
        '''
        
        Reset all values in the hand 
        
        '''
        if self.cards_list:
            del self.cards_list
        self.cards_list = list()

        self.rank = None
        self.hand = None
        self.strength = 'not_set'
        self.score    = 'not_set'

        self.repeats  = defaultdict(list)
        self.flushes  = {'C': [], 'H': [], 
                         'D': [], 'S': []}

        self.zero = lambda: 0
        self.ranks_by_count = defaultdict(list)
        self.counts_by_rank = defaultdict(self.zero)

        # We keep a track of which straights we have by listing the 
        # ranks we have. E.g. False, False, 2, 3, 4, 5, 6, False,... 
        self.straightRanks = \
            lambda: [False for i in range(len(self.RANKS))]
        self.straight_flushes = {'C': self.straightRanks(),
                                 'H': self.straightRanks(),
                                 'D': self.straightRanks(),
                                 'S': self.straightRanks(),
                                 'X': self.straightRanks()}



    def addCard(self, card):
        '''
        
        Add a card to the hand.
        
        '''
        self.cards_list.append(card)

        self.repeats[card.rank].append(card)
        self.flushes[card.suit].append(card)

        # Handle repeats
        count = self.counts_by_rank[card.rank] + 1
        self.counts_by_rank[card.rank] = count
        self.ranks_by_count[count].append(card.rank)
        if count > 1:
            self.ranks_by_count[count-1].remove(card.rank)
        
        # Handle straights
        ridx = self.RANKS.index(card.rank)
        self.straight_flushes[      'X'][ridx] = card
        self.straight_flushes[card.suit][ridx] = card

        
        
    def scrapeStraights(self, suit=None):
        '''

        Find all straights currently in hand.

        '''
        straight_ranks = self.straight_flushes[suit]

        temp   = []
        result = []

        for i in range(len(straight_ranks)):
            if straight_ranks[i]:
                temp.append(straight_ranks[i])
            else: 
                if len(temp)>=2: 
                    result.append(temp)

                temp = []               

        if len(temp) >= 2: result.append(temp)

        return result



    def scrapeFlushes(self, flushes):
        ''' 
        
        Find all flushes currently in hand.
        
        '''
        flush = None
        for suit in self.SUITS:
            if len(flushes[suit]) >= 5:
                flush = flushes[suit]
                break
        if not flush: return []
        else:
            result = [flush[i:i+5] for i in range(len(flush)-4)]
        return result
        
        
        
    def fillInHighs(self):
        '''
        
        Auxiliary to findRank
        
        '''
        excluded_ranks = [card.rank for card in self.hand]
        if len(self.hand) < 5:
            for key in self.RANKS:
                if self.repeats[key]:
                    if self.repeats[key][0].rank not in excluded_ranks:
                        self.hand.append(self.repeats[key][0])
                    if len(self.hand) == 5: break    
        
    

    def findRank(self):
        '''
        
        Find the highest rank that can be constructed from the given hand.
        
        '''
        
        # Check for straight flushes
        for suit in self.SUITS:
            suit_straights = self.scrapeStraights(suit)
            for s in suit_straights:
                if len(s) >= 5:
                    if s[0].rank == 'A':
                        self.rank = "Royal Flush"
                    else: 
                        self.rank = "Straight Flush"
                    self.hand = s[:5]
                    return None
        
        repeats = defaultdict(list)
        for key in self.ranks_by_count:
            for r in self.ranks_by_count[key]:
                repeats[key].append(self.repeats[r])
                

        if repeats[4]:
            self.rank = "Quad"
            self.hand = repeats[4][0]
            self.fillInHighs()
            return None
            
        if repeats[3] and repeats[2]:
            self.rank = "Full House"
            self.hand = repeats[3][0] + \
                        repeats[2][0]
            return None

        flushes   = self.scrapeFlushes(self.flushes)            
        if flushes:
            self.rank = "Flush"
            self.hand = flushes[0]
            return None

        straights = [s for s in self.scrapeStraights('X') if len(s) >= 5]
        if straights:
            self.rank = "Straight"
            self.hand = straights[0][:5]
            return None
            
        if repeats[3]:
            self.rank = "Trip"
            self.hand = repeats[3][0]
            self.fillInHighs()
            return None
            
        if len(repeats[2]) > 1:
            self.rank = "Two Pair"
            self.hand = repeats[2][0] + \
                        repeats[2][1]
            self.fillInHighs()
            return None
            
        if repeats[2]:
            self.rank = "Pair"
            self.hand = repeats[2][0]
            self.fillInHighs()
            return None
            
        else:
            self.rank = "High"
            self.hand = []
            self.fillInHighs()


    
    def findStrength(self):
        ''' 

        Count the number of hands weaker than self.
        
        '''
        self.strength = countWeakerHands(self)


    
    def findScore(self):
        '''
        
        Calculate the fraction of cards weaker than hand
        
        '''
        if self.strength == 'not_set': self.findStrength()
        self.score = percentageScore(self) 


    
if __name__ == '__main__':

    import hand_test
