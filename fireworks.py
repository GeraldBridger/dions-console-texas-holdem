from os import system
from time import sleep

f1 = "     \n  &  \n     "
f2 = "  *  \n *** \n  *  "
f3 = " *** \n** **\n *** "
f4 = " ... \n.. ..\n ... "
f5 = " ,,, \n,, ,,\n ,,, "
f6 = "   , \n,   ,\n ,   "
f7 = "   , \n     \n ,   "

g1=  '\n\n\n\n\n      &'
      
g2 = r"""



      *      
     ***   
      *"""

g3 = r"""


      *                         
     *|*    
    *-o-*                        
     *|*    
      *"""

g4 = r"""

      *       
    * * *    
   **\|/**  
   **-O-**
   **/|\**  
    * * *    
      *"""

g5 = r"""
    ****** 
  ***    ***       
 **   ;;   **          
**  ;;  ;;  **    
** ;;    ;; **       
**  ;;  ;;  **
 **   ;;   **
  ***    ***
    ******"""
    
g6 =r"""
    ;;;;;; 
  ;;;    ;;;       
 ;;   ;    ;;
;;   ;   ;  ;;
;; ;     ;  ;;
;;   ;  ;   ;;
 ;;   ;;   ;;
  ;;;    ;;;
    ;;;;;;"""    

g7 =r"""
    ;;;;;; 
  ;;      ;;       
 ;    ;     ;
;;   ;   ;  ;;
;        ;  ;;
;;   ;       ;
 ;;    ;   ;;
  ;;      ;;
    ;;;;;;"""          
    
g8 =r"""
    ;;;;;; 
  ;       ;;       
 ;    ;     ;
;;   ;   ;  ;;
;        ;   ;
;;   ;       ;
 ;     ;    ;
  ;        ;
    ;;;;;;""" 
    
g9 =r"""
    ; ; ;  
  ; ;     ;       
  ;   ;    ; 
;        ;   ;
;  ;        ; 
 ;           ;
  ;    ;    ;
  ;        ;
    ;; ; ;"""       
    
g10 =r"""
    ; ; ;  
  ;       ;       
           ; 
;        ;   ;
;  ;          
             ;
  ;    ;    ;
  ;        ;
    ;  ; ;"""       
    
g11 =r"""
    ;   ;  
  ;               
            
;           ;
             
 ;          ;
             
  ;       ;
      ;  """     
    
sequence1 = (f1, f2, f3, f4, f5, f6, f7)
sequence2 = (g1, g2, g3, g4, g5, g6, g7,g8,g9,g10,g11)
sequence = sequence1
while True: 
    sequence = sequence1 if sequence == sequence2 else sequence2
    for frame in sequence:
        print(frame)
        sleep(0.05)
        system('cls')
    sleep(1)