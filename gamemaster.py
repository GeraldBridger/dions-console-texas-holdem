#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''

  Single-class input processing & state modification  
  ('gameplay') module for playing a round of poker    

'''

import time

from random        import shuffle
from collections   import defaultdict
from utilities     import orderedDeck


class GameMaster:

    '''

    Input processing & state modification ('gameplay')

    '''

    def __init__(self, game_screen, players, delay=1):

        self.game_screen  = game_screen
        self.deck         = list() # initialized by 1st call to roundReset

        self.dealer       = 0 #None
        self.small_blind  = {'player': '', 'amount': 1}
        self.big_blind    = {'player': '', 'amount': 2}

        self.minimum_raise = 0
        self.previous_bet  = 0
        self.pooled_bets   = 0

        self.players       = players
        self.flop          = list()
        self.bet_placers   = list()
        self.first_better  = None
    
        self.DELAY = delay   # slow game down so human can follow along
        


    def RoundMaster(self):
        '''

        Defines how a game of poker is played.

        '''
        self.roundReset() # initialize everything
        
        game_on = lambda: len(self.players) > 1 and \
                True in {p.is_human for p in self.players}

        while game_on():
            self.game_screen.draw()
            
            self.getForcedBets()
            self.dealHoles()
            self.dealTableCards()

            self.game_screen.revealHands()
            self.game_screen.draw()

            winners  = self.findWinners()
            payments = self.paidWinners(winners)

            self.declarePayments(payments)
            self.roundReset()



    def roundReset(self):
        '''
        Clear all variables relating to the round state.
        First function to run and runs on every new round.
        '''
        self.deck = orderedDeck()
        shuffle(self.deck)

        # `players` exists strictly so that we can 
        # loop over it as we modify self.players
        players = list(self.players)

        
        #########################################
        ### DO NOT FINGERPOKEN THE CODE BELOW ###
        #########################################

        # Remove bust players
        for p in players:
            if p.money > 0:
                p.hand.resetHand()
                p.total_bet = 0
            else:
                if self.players.index(p) < self.dealer:
                    self.dealer -= 1
                p.status = 'bust'
                self.players.remove(p)
                self.game_screen.marqueeSetBust(p)

        # Reset game state
        self.flop          = list()
        # bet_placers needs to be a copy to avoid mangling self.players
        self.bet_placers   = list(self.players) 
        self.pooled_bets   = 0
        self.previous_bet  = 0
        self.minimum_raise = 0

        self.rotateDealer()

        self.game_screen.roundReset(players)


        
    def rotateDealer(self):
        '''

        Set dealer, blinds and first better to the 
        next player down the sequence. 

        '''
        self.dealer       = (self.dealer + 1) % len(self.players)
        next_sb           = (self.dealer + 1) % len(self.players)
        next_bb           = (self.dealer + 2) % len(self.players)
        self.first_better = (self.dealer + 3) % len(self.players)

        self.small_blind['player'] = self.players[next_sb]
        self.big_blind['player']   = self.players[next_bb]



    '''' ~= Dealing & Betting =~ '''

    def takeBet(self, player, bet):
        '''

        Get bet from a betting player.
        Auxiliary to effectuatePlayerChoice

        '''
        player.money          -= bet
        player.total_bet      += bet

        self.pooled_bets      += bet
        self.game_screen.pooled_bets = self.pooled_bets
        self.last_players_bet  = player.total_bet

        return bet



    def effectuatePlayerChoice(self, player):
        '''

        Get the player's choice and change game 
        & player's variables to reflect it.

        '''

        # We pass 'self' (the game) to player object to
        # allow it access to the information it needs to
        # decide what action to take or how much to bet.
        chosen_action = player.getAction(self, self.game_screen)

        # aliases
        takeBet = self.takeBet
        name = player.name
        bet = 0

        # Undisplay available actions
        self.game_screen.command_string = "\n" 
        self.game_screen.draw()

        if chosen_action == 'Check': 
            pass

        elif chosen_action in ('Bet', 'Raise'):
            raised_bet = player.makeBet(self, self.minimum_raise, 
                                        chosen_action=='Raise')
            self.minimum_raise = raised_bet
            bet = self.takeBet(player, raised_bet)

        elif chosen_action == 'Call':
            amount = self.last_players_bet - player.total_bet
            bet    = takeBet(player, amount)

        elif chosen_action == 'All in':
            bet = takeBet(player, player.money)

            if bet > self.minimum_raise:
                self.minimum_raise = bet

        elif chosen_action == 'Fold':
            player_index = self.bet_placers.index(player)
            if player_index < self.first_better:
                self.first_better -= 1
            self.bet_placers.remove(player)

        self.game_screen.drawAction(name, chosen_action, bet)


  
    def getForcedBets(self):
        '''

        Get small blind and big blind bets.

        '''
        takeBet = self.takeBet

        player = self.small_blind['player'] 
        amount = self.small_blind['amount'] 
        bet = amount if player.money >= amount else player.money
        takeBet(player, bet)
        self.game_screen.drawAction(player.name, 'small_blind', bet)

        player = self.big_blind['player']
        amount = self.big_blind['amount']
        bet = amount if player.money >= amount else player.money
        takeBet(player, bet)
        self.game_screen.drawAction(player.name, 'big_blind', bet)

        self.minimum_raise = self.big_blind['amount']



    def endBettingRound(self):
        '''

        Determine if all players have placed an equal total bet.
        Auxiliary to placeBets:
            used to determine if a round of betting should end.

        '''
        players = self.bet_placers
        # adding the clause of having money might make mischief
        bets    = set([player.total_bet for player in players \
                       if player.money > 0 ])
        monies  = set([player.money     for player in players])
        
        return len(bets) == 1 or monies == set([0])


    
    def placeBets(self):
        '''

        Have all players place their bets.

        '''
        bet_placers = self.bet_placers

        while True:
            print('!')
            # Remove players who folded from bet sequence
            bet_sequence = bet_placers[self.first_better:] + \
                           bet_placers[:self.first_better]
            
            for player in bet_sequence:
                #print(player.name, [p.name for p in self.bet_placers])
                if len(self.bet_placers) == 1:
                    #print("breaking...")
                    break
                #print("effectuating player choice...")
                self.effectuatePlayerChoice(player)
                #print("player choice effectuated...")
                #print(player.name, [p.name for p in self.bet_placers])
                #input("...")
            if len(self.bet_placers) <= 1 or self.endBettingRound():
                break
            #print("...")

    
    def dealHoles(self):
        '''

        Deal cards to players and take their bets.

        '''

        sequence = [player for player in self.players]
        sequence = sequence[self.dealer:] + sequence[:self.dealer]

        for player in sequence:
            # No need for DRY here, we only need this twice
            for i in range(2):
                card = self.deck.pop()
                player.hand.addCard(card)
                self.game_screen.marqueeCardAppend(player, card)
                self.game_screen.draw()

                time.sleep(self.DELAY)

        self.placeBets()


            
    def dealTableCards(self):
        '''

        Deal community cards, taking bets after each card.

        '''
        # Flop
        for card in (self.deck.pop(), 
                     self.deck.pop(), 
                     self.deck.pop()):

            for player in self.players:
                player.hand.addCard(card)

            self.flop.append(card)
            self.game_screen.flop.append(card)

        self.game_screen.draw()
        self.placeBets()
        
        # Turn and River
        while len(self.bet_placers) > 1:

            card = self.deck.pop()

            # Unsure about the following couplet
            for player in self.players:
                player.hand.addCard(card)

            self.flop.append(card)

            self.game_screen.flop.append(card)
            self.game_screen.draw()

            if  len(self.flop) == 5:
                break

            self.placeBets()


        
    ''' ~= Hand Comparison =~ '''
    
    def findWinners(self):
        '''

        Compare hands and return highest ranking players.

        '''
        best_score = 0
        last_rank  = ''
        candidates = list()
        
        for player in self.bet_placers:

            player.hand.findRank()
            player.hand.findStrength()

            rank  = player.hand.rank
            score = player.hand.strength

            #print(player.name, rank)
            
            if score == best_score:
                #candidates.append(score) # Why is this here?
                candidates.append(player)

            elif rank == last_rank == 'Royal Flush':
                #candidates.append(score)
                candidates.append(player)

            elif score > best_score:
                best_score = score
                candidates = [player]

        from player import Player
        assert set(map(type, candidates)) == {Player,}
        return candidates


        
    ''' ~= Player Payment =~ '''
    
    def paidWinners(self, payees):
        '''

        Pay the players in the proportions appropriate to them.

        '''
        # print(payees)
        # input(">>>")
        payments_made = {} # Keep track of payments made
                           # So we can report it back to user
        payment = int(self.pooled_bets * 1/len(payees))        

        for player in payees:

            self.pooled_bets -= payment        
            player.money     += payment
            payments_made[player.name] = { 'player'  : player, 
                                           'payment' : payment }
        if self.pooled_bets > 0: 

            payees[0].money  += self.pooled_bets
            self.pooled_bets  = 0

        return payments_made



    def declarePayments(self, payees):
        '''

        Print a message declaring each given player a winner.

        '''
        for name in payees:
            #print(payees[name])
            hand = payees[name]['player'].hand
            #print(hand)
            hand_string = ' '.join([card.rank+card.suit for card in 
                                    hand.cards_list])

            payment_declared = \
                name + " has won " + self.game_screen.currency_symbol  + \
                str(payees[name]['payment']) + " with "                + \
                payees[name]['player'].hand.rank                       + \
                ' ' + hand_string + '\n'

            self.game_screen.message_string += payment_declared

        self.game_screen.draw()

        if True in {p.is_human for p in self.players}:
            input("Press Enter to continue...")
        self.game_screen.message_string = ''
    
