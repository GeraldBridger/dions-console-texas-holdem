###################################################################
#                                                                 #
#  Single-class module for arranging content displayed to screen  #
#                                                                 #
###################################################################

####   GameScreen FORMAT:   ####
#   header
#   
#   player_marquees['player0']
#   player_marquees['player1']
#   ...
#   player_marquees['playerN']
#
#   table_cards
#   pooled_bets
#
#   command_string
#   'Enter a command to continue...'
#   > 

import os
from collections import namedtuple
from collections import OrderedDict
from utilities   import card

class GameScreen:
    '''

    Construct, manage and print strings representing the game state.

    '''
    
    def __init__(self, header):
        # Initialize player_marquees as OrderedDict to draw marquees in order
        ## Screen sections (in order of appearance)
        self.header          = header    
        self.player_marquees = OrderedDict()
        self.flop            = []
        self.pooled_bets     = 0
        self.message_string  = ''
        self.command_string  = ''     
        ## Screen formatting 
        self.name_field_len = len('IsaacJacob') # 10
        self.cash_field_len = len('§99999')     # 6
        self.hand_field_len = len('XX XX')     # 6
        self.currency_symbol = '§'
        
    ### Marquee Formatting ###
        
    def formatedPlayerName(self, player_name):
        '''Pad or cut a player's name'''
        field_len = self.name_field_len
        return player_name[:field_len] + ' ' * (field_len-len(player_name))
        
    def formatedPlayerMoney(self, player_money):
        '''Pad or cut a player's money (string)'''
        field_len = self.cash_field_len
        money_string = self.currency_symbol+player_money
        return money_string[:field_len] + ' '*(field_len-len(money_string))

    def formatedPlayerHand(self, hand, show_hand=True):
        '''String a hand of cards together for display in a marquee.'''
        if show_hand:
            result  = ' '.join([str(card.rank)+card.suit for card in hand])
        else: 
            result  = ' '.join(['XX' for card in hand])
        result += ' '*(self.hand_field_len-len(result))
        return result
        
    def formatedPlayerBet(self, player_bet):
        '''Pad or cut a player's bet (string)'''
        field_len = self.cash_field_len
        if player_bet == 0:
            return ''
        else: 
            bet_string  = self.currency_symbol + str(player_bet)
            return bet_string[:field_len] + ' '*(field_len-len(bet_string))
        
    def formatedPlayerMarquee(self, name):
        '''String player information stored in a marquee together.'''
        marquee = self.player_marquees[name]
        hand        = marquee['hand']
        money       = marquee['money']
        total_bet   = marquee['total_bet']
        current_bet = marquee['current_bet']
        show_hand   = marquee['show_hand']
        action_taken = marquee['action_taken'] 
        line =       (self.formatedPlayerName(name),             \
                      self.formatedPlayerHand(hand, show_hand),  \
          'Money: ' + self.formatedPlayerMoney(str(money)),      \
          'Bet: '   + self.formatedPlayerMoney(str(total_bet)),  \
          action_taken + self.formatedPlayerBet(current_bet)+'\n')
        return ' '.join(line)
        
        
        
    ''' ~= Screen Drawing =~ '''
    
    def draw(self):
        '''

        Draw all sections of the GameScreen to screen.

        '''
        os.system('cls' if os.name == 'nt' else 'clear')
        result  = self.header
        result += '\n\n'
        for name in self.player_marquees:
            result += self.formatedPlayerMarquee(name)
        result += '\n'
        result += 'Table cards: ' + self.formatedPlayerHand(self.flop)
        result += '\n'
        result += 'Bet pool:    ' + self.currency_symbol + str(self.pooled_bets)
        result += '\n'
        result += self.message_string
        result += '\n'
        result += self.command_string
        print(result)



    def drawAvailableActions(self, player, actions):
        '''
        
            If player is human, draw his available actions to screen.
            Auxiliary to placeBets.
        
        '''
        options = ""
        j = int(not 'Fold' in actions)
        
        for i in range(len(actions)):
            options += "{0}: {1}  ".format(i+j, actions[i])
        
        self.command_string = options
        self.draw()



    def drawAction(self, name, action, current_bet):
        '''

        Change marquee to reflect a player's bet.

        '''
        assert isinstance(current_bet, int)
        self.player_marquees[name]['action_taken']  = self.actions_dict[action]
        self.player_marquees[name]['current_bet' ]  = current_bet
        self.player_marquees[name]['total_bet'   ] += current_bet
        self.player_marquees[name]['money'       ] -= current_bet


        
    ### Marquee Creation & Manipulation ###
    
    def addPlayerMarquee(self, player): 
        '''Add a marquee used to display information about the player.'''
        show_hand = player.is_human
        marquee = {'money':        player.money, \
                   'hand':         player.hand.cards_list,  \
                   'show_hand':    show_hand,    \
                   'total_bet':    0,          \
                   'action_taken': '',           \
                   'current_bet':  0}
        self.player_marquees[player.name] = marquee



    def marqueeCardAppend(self, player, card):
        '''

        Add card to those displayed alongside the given player name.

        '''
        self.player_marquees[player.name]['hand'].append(card)
   


    def revealHands(self):
        '''

        Set a marquee to reveal the cards in Player's hands

        '''
        for name in self.player_marquees:
            self.player_marquees[name]['show_hand'] = True

    actions_dict = {'bet': 'Bet ', 'Bet': 'Bet ', \
                    'small_blind': 'SB: ', 'big_blind': 'BB: ', \
                    'raise': 'Raised: ', 'Raise': 'Raised: ',   \
                    'call': 'Called: ', 'Call': 'Called: ',     \
                    'fold': 'Folded', 'Fold': 'Folded', \
                    'check': 'Check', 'Check': 'Check',
                    'All in': 'ALL IN ', '': '='}



        ### Screen State Clearing ###    
        
    def marqueeRoundReset(self, player):
        '''Reset bet and money sections of player's marquee.''' 
        self.player_marquees[player.name]['action_taken'] = ''
        self.player_marquees[player.name]['current_bet']  = 0
        self.player_marquees[player.name]['total_bet']    = 0
        self.player_marquees[player.name]['show_hand']    = True if player.is_human else False
        self.player_marquees[player.name]['money']        = player.money
        self.player_marquees[player.name]['hand']         = list()
        
    def roundReset(self, players):
        '''Reset table cards.'''
        self.flop = list()
        self.pooled_bets = 0
        for player in players:
            if player.status != 'bust': self.marqueeRoundReset(player)
            else:                       self.marqueeSetBust(player)
        
    def marqueeSetBust(self, player):
        '''Set a player's marquee to reflect the fact that he is bust.'''
        self.player_marquees[player.name] =                \
            {'money':        'BUST!',                      \
             'hand':         [card(rank='-', suit='-')]*2, \
             'show_hand':    True,                         \
             'total_bet':    0,                           \
             'action_taken': '',                           \
             'current_bet':  0}
